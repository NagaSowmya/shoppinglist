import { Ingredient } from "../models/ingredient";
import { Recipes } from "../models/recipe";

export class RecipesService{
    private recipes:Recipes[]=[];
addRecipe(title:string,description:string,difficulty:string,ingredients:Ingredient[]){
    this.recipes.push(new Recipes(title,description,difficulty,ingredients));
    console.log(this.recipes);
}
getRecipes(){
    return this.recipes.slice();
}
updateRecipe(index:number,title:string,description:string,difficulty:string,ingredients:Ingredient[]){
    this.recipes[index]=new Recipes(title,description,difficulty,ingredients);
}
removeRecipe(index:number){
    this.recipes.splice(index,1);
}
}